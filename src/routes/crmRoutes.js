const routes = (app) =>{
    app.route('/contact').get((req, res)=>{
        res.send('Get Request');
    }).post((req,res)=>{
        res.send('post req');
    });

    app.route('/contact/:contactID').put((req, res, next)=>{
        console.log(`request from: ${req.originalUrl}`);
        console.log(`request method ${req.method}`)
        res.send('Put method call ');
        next();
    }).delete((req, res)=>{
        res.send('delete function')
    })
}
export default routes;