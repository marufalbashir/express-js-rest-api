//import theModule from './a-file.js';

import express from 'express';
import bodyParser from 'body-parser';
//import { json } from 'body-parser'; 
import routes from './src/routes/crmRoutes.js'
const app = express();
const PORT = 3000;
app.listen(PORT, ()=>{
    console.log(`Welcome Express JS . App Port is ${PORT}`);
});

// routes
routes(app);

app.get('/',(req, res)=>{
    res.send(`Welcome Express JS . Your App is Running on the port ${PORT}`)
})
app.post('/', (req,res) =>{
    if(req.username == 1234){
        res.send('thank you MAruf');
    }
    else{
        res.send('UserName Not Matched');
    }
})